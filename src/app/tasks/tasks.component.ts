import { Component, OnInit } from '@angular/core';
import {Task} from "../Task";
import {TaskService} from "../services/task.service";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks: Task[] = [];

  constructor(private taskService: TaskService ) { }

  ngOnInit(): void {
    this.tasks = this.taskService.getTasks()
  }
  addTask(newTask: Task) {
     this.tasks.push(newTask);
  }
  onDeleteTask(index: number) {
    this.tasks.splice(index, 1);
  }
}
