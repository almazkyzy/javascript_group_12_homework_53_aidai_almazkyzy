import {Task} from './Task'
export const TASKS: Task[] = [
  {
    id: 1,
    text: 'Buy milk',
    day: 'Nov 2, 2021'
  },
  {
    id: 2,
    text: 'Understand Angular',
    day: 'Nov 3, 2021'
  },
  {
    id: 3,
    text: 'Complete Angular Project',
    day: 'Nov 4, 2021'
  },

]
