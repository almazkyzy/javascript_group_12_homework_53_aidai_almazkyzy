import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Task} from "../Task";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask:EventEmitter<Task> = new EventEmitter<Task>();
  text: string ='' ;
  day: string = '';

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(){
    if (!this.text) {
      alert('add the task dear!');
      return;
    }
    const newTask = {
      text: this.text,
      day: this.day,
    };
    this.onAddTask.emit(newTask)
    this.text = '';
    this.day = '';
  }
}
